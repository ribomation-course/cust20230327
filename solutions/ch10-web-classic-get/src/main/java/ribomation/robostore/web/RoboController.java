package ribomation.robostore.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.jpa.JpaRobotDAO;

import java.util.Properties;

@Controller
@RequestMapping("/robo")
public class RoboController {
    private final JpaRobotDAO dao;

    public RoboController(JpaRobotDAO dao) {
        this.dao = dao;
    }

    @GetMapping
    public String index() {
        return "redirect:/robo/list";
    }

    @GetMapping("summary")
    public String summary(Model m) {
        m.addAttribute("count", dao.count());
        m.addAttribute("total", dao.totalValue());
        m.addAttribute("zero", dao.countByCountEquals(0));
        return "summary";
    }

//    @GetMapping("list")
//    public String list(Model m) {
//        m.addAttribute("list", dao.findAll(Pageable.ofSize(10)));
//        return "list";
//    }

    @GetMapping("show/{name}")
    public String show(Model m, @PathVariable String name) {
        m.addAttribute("obj", dao.findById(name).orElseThrow(() -> new NotFound(name)));
        return "show";
    }

    @GetMapping("search")
    public String search(Model m, @RequestParam String phrase) {
        m.addAttribute("list", dao.findFirst5ByNameLikeIgnoreCase('%' + phrase + '%'));
        return "list";
    }

    @GetMapping({"list", "list/{action}", "list/{action}/{page}"})
    public String list(Model m,
                       @PathVariable(required = false) PageAction action,
                       @PathVariable(required = false) Integer page) {
        var nav = Pagination.of(page, (int) dao.count()).paginate(action);
        var pr = PageRequest.of(nav.page() - 1, nav.size());
        m.addAttribute("list", dao.findAll(pr));
        m.addAttribute("page", nav.page());
        return "list";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String error(Model m, NotFound x) {
        m.addAttribute("name", x.name);
        return "error";
    }

}
