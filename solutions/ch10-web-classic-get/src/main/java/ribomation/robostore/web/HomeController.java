package ribomation.robostore.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ribomation.robostore.AppInfo;

import java.util.ArrayList;
import java.util.Properties;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeController {
    private final AppInfo app;
    private final Properties buildData;

    public HomeController(AppInfo app, Properties buildData) {
        this.app = app;
        this.buildData = buildData;
    }

    @GetMapping
    public String index() {
        return "redirect:/home";
    }

    @GetMapping("home")
    public String home(Model m) {
        var build = buildData.entrySet().stream()
                .map(e -> new Pair(e.getKey().toString(), e.getValue().toString()))
                .toList();
        m.addAttribute("app", app);
        m.addAttribute("build", build);
        return "home";
    }

    @Data
    @AllArgsConstructor
    public static class Pair{
        String key;
        String val;
    }
}
