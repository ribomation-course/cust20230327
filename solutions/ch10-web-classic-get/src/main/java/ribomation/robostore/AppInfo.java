package ribomation.robostore;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
@ConfigurationProperties(prefix = "robostore")
@Data
@NoArgsConstructor
public class AppInfo {
    String name;
    String version;
    String author;
}
