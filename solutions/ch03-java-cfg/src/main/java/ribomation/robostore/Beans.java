package ribomation.robostore;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.domain.RobotDAO;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.H2DatasourceFactory;
import ribomation.robostore.jdbc.JdbcRobotDAO;

import javax.sql.DataSource;

@Configuration
public class Beans {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propsConfig() {
        var cfg = new PropertySourcesPlaceholderConfigurer();
        cfg.setLocation(new ClassPathResource("/db.properties"));
        return cfg;
    }

    @Bean
    public BeanPreparator mapper() {
        return new BeanPreparator();
    }

    @Bean
    DataSource ds(
            @Value("${jdbc.url}") String url,
            @Value("${jdbc.username}") String usr,
            @Value("${jdbc.password}") String pwd
    ) {
        return new H2DatasourceFactory(url, usr, pwd).create();
    }

    @Bean
    public RobotDAO dao(DataSource ds, BeanPreparator mapper) {
        return new JdbcRobotDAO(ds, mapper);
    }

    @Bean
    public RoboCommands commands(RobotDAO dao) {
        return new RoboCommands(dao);
    }
}
