package ribomation.robostore.jdbc;

import ribomation.robostore.domain.Robot;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BeanPreparator {
    private Robot robo;

    public void setRobo(Robot robo) {
        this.robo = robo;
    }

    public void setValues(PreparedStatement ps) throws SQLException {
        if (robo == null) {
            throw new IllegalArgumentException("Missing robo object");
        }
        setValues(ps, robo);
        robo = null;
    }

    public void setValues(PreparedStatement ps, Robot robo) throws SQLException {
        System.out.printf("[setVal] robo=%s%n", robo);
        try {
            int idx = 1;
            for (var name : Robot.names) {
                var field = Robot.class.getDeclaredField(name);
                field.setAccessible(true);
                var type = field.getType();
                var value = field.get(robo);
                System.out.printf("[setVal] idx=%d, type=%s, value='%s'%n", idx, type, value);
                if (type.equals(String.class)) {
                    ps.setString(idx, value.toString());
                } else if (type.equals(Integer.class) || type.equals(int.class)) {
                    ps.setInt(idx, (Integer) value);
                } else if (type.equals(Float.class) || type.equals(float.class)) {
                    ps.setFloat(idx, (Float) value);
                }
                ++idx;
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public Robot mapRow(ResultSet rs) {
        try {
            return Robot.builder()
                    .name(rs.getString("name"))
                    .price(rs.getFloat("price"))
                    .count(rs.getInt("count"))
                    .description(rs.getString("description"))
                    .uid(rs.getString("uid"))
                    .city(rs.getString("city"))
                    .country(rs.getString("country"))
                    .icon(rs.getString("icon"))
                    .image(rs.getString("image"))
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
