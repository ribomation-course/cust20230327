package ribomation.robostore.spring;

import org.springframework.stereotype.Service;
import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.domain.RobotDAO;

@Service
public class SpringRoboCommands extends RoboCommands {
    public SpringRoboCommands(RobotDAO dao) {
        super(dao);
    }
}
