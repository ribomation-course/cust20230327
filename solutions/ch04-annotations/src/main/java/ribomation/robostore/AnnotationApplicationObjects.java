package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ribomation.robostore.cli.RoboCommands;

public class AnnotationApplicationObjects extends ApplicationObjects {
    private final ApplicationContext ctx;

    public AnnotationApplicationObjects() {
        var ctx = new AnnotationConfigApplicationContext();
        ctx.scan("ribomation.robostore.spring");
        ctx.refresh();
        this.ctx = ctx;
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }
}
