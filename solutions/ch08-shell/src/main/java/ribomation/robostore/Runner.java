package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import ribomation.robostore.cli.RoboCommands;

import java.util.Properties;

public class Runner {
    private final ApplicationContext ctx;
    public Runner(ApplicationContext ctx) {this.ctx = ctx;}

    public void run(String[] args) {
        var cmd = ctx.getBean(RoboCommands.class);
        if (args.length == 0) {
            cmd.summary();
        } else {
            switch (args[0]) {
                case "build" -> cmd.build();
                case "summary" -> cmd.summary();
                case "list" -> cmd.list();
                case "first" -> cmd.listFirst();
                case "last" -> cmd.listLast();
                case "next" -> cmd.listNext(Integer.valueOf(args[1]));
                case "prev" -> cmd.listPrev(Integer.valueOf(args[1]));

                case "one" -> cmd.one(args[1]);
                case "delete" -> cmd.delete(args[1]);
                case "search" -> cmd.search(args[1]);

                case "update" ->
                        cmd.update(args[1], args[2], args[3], Float.valueOf(args[4]), Integer.valueOf(args[5]));
                case "create" ->
                        cmd.create(args[1], args[2], args[3], args[4], Float.valueOf(args[5]), Integer.valueOf(args[6]));
            }
        }
    }
}

