import { loadById, remove } from '/js/rest-client.js'

function populateCard(card, item) {
    card.innerHTML = `
    <img src="${ item.image }" class="card-img-top" alt="${ item.name }">
    <div class="card-body">
        <h5 class="card-title text-center">${ item.name }</h5>
        <h5 class="card-subtitle text-center text-capitalize fst-italic">${ item.description }</h5>
        <table class="table table-borderless table-sm card-text">
            <tr>
                <th>Price</th>
                <td>${ item.price } kr</td>
            </tr>
            <tr>
                <th>Stock Count</th>
                <td>${ item.count }</td>
            </tr>
            <tr>
                <th>UID</th>
                <td><code>${ item.uid }</code></td>
            </tr>
            <tr>
                <th>Origin</th>
                <td>${ item.city }, ${ item.country }</td>
            </tr>
        </table>
    </div>
    <div class="card-footer text-center">
        <a href="/robo/edit/{{name}}" class="btn btn-primary">
            <i class="bi bi-pencil-square"></i> Edit
        </a>
        <a href="#" class="remove btn btn-danger">
            <i class="bi bi-trash-fill"></i> Remove
        </a>
    </div>
    `;
}

function mkRemoveHdlr(name) {
    return async (ev) => {
        ev.preventDefault();
        await remove(name);
        let url = location.href;
        url = url.substring(0, url.indexOf('/show')) + '/list';
        location.assign(url);
    };
}

(async () => {
    const card = document.querySelector('#ajax')
    const name = card.dataset.name;
    const item = await loadById(name);
    populateCard(card, item);

    const removeBtn = document.querySelector('.remove');
    removeBtn.addEventListener('click', mkRemoveHdlr(name));
})()

