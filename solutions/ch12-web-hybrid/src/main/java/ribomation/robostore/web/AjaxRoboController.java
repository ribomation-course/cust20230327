package ribomation.robostore.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.Robot;
import ribomation.robostore.jpa.JpaRobotDAO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/robo")
public class AjaxRoboController {
    private final JpaRobotDAO dao;
    public AjaxRoboController(JpaRobotDAO dao) {
        this.dao = dao;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String,String> error(NotFound x) {
        return Map.of("name", x.name);
    }

    @GetMapping
    public List<Robot> list() {
        return dao.findAll(PageRequest.of(0, 8)).toList();
    }

    @GetMapping("/{name}")
    public Robot one(@PathVariable String name) {
        return dao.findById(name).orElseThrow(() -> new NotFound(name));
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable String name) {
        dao.deleteById(name);
    }

}
