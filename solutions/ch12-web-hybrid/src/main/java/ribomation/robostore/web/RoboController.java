package ribomation.robostore.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.jpa.JpaRobotDAO;

import java.util.Properties;

@Controller
@RequestMapping("/robo")
public class RoboController {
    private final JpaRobotDAO dao;

    public RoboController(JpaRobotDAO dao) {
        this.dao = dao;
    }

    @GetMapping
    public String index() {
        return "redirect:/robo/list";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String error(Model m, NotFound x) {
        m.addAttribute("name", x.name);
        return "error";
    }

    @GetMapping("list")
    public String list() {return "list";}

    @GetMapping("create")
    public String create() {return "create";}

    @GetMapping("show/{name}")
    public String show(Model m, @PathVariable String name) {
        m.addAttribute("name", name);
        return "show";
    }

    @GetMapping("edit/{name}")
    public String edit(Model m, @PathVariable String name) {
        m.addAttribute("name", name);
        return "edit";
    }

    @GetMapping("summary")
    public String summary(Model m) {
        m.addAttribute("count", dao.count());
        m.addAttribute("total", dao.totalValue());
        m.addAttribute("zero", dao.countByCountEquals(0));
        return "summary";
    }

    @GetMapping("search")
    public String search(Model m, @RequestParam String phrase) {
        m.addAttribute("list", dao.findFirst5ByNameLikeIgnoreCase('%' + phrase + '%'));
        return "list";
    }

    @PostMapping("update/{name}")
    public String update(@PathVariable String name, UpdateDTO dto) {
        var obj = dao.findById(name).orElseThrow(() -> new NotFound(name));
        dao.save(dto.patch(obj));
        return "redirect:/robo/show/" + name;
    }

    @PostMapping("save")
    public String save(CreateDTO dto) {
        var name = dto.name;
        dao.save(dto.toRobo());
        return "redirect:/robo/show/" + name;
    }

}
