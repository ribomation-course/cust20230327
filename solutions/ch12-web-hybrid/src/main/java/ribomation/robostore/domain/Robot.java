package ribomation.robostore.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;

@Entity
@Table(name = "ROBO_STORE")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Data
@Builder
@FieldNameConstants(asEnum = true, innerTypeName = "namesEnum")
public class Robot {
@Id private String name;
    private String description;
    private String city;
    private String country;
    private Float price;
    private Integer count;
    private String uid;
    private String icon;
    private String image;

    public static final String[] names =
            Stream.of(namesEnum.values())
                    .map(Enum::name)
                    .toArray(String[]::new);

    public Robot complete() {
        if (missing(name)) throw new IllegalArgumentException("missing name");
        if (missing(image)) image = mkImage(name);
        if (missing(icon)) icon = mkIcon(name);
        if (missing(uid)) uid = mkUID(name);
        if (missing(price)) price = 10F;
        if (missing(count)) count = 1;
        if (missing(description)) description = "Just a silly description";
        if (missing(city)) city = "Stockholm";
        if (missing(country)) country = "Sweden";
        return this;
    }

    private String mkUID(String s) {
        try {
            var alg = MessageDigest.getInstance("SHA-256");
            var bytes = s.getBytes(StandardCharsets.UTF_8);
            var digest = alg.digest(bytes);
            var num = new BigInteger(1, digest);
            var hex = new StringBuilder(num.toString(16));
            while (hex.length() < 64) hex.insert(0, '0');
            return hex.substring(0, 32).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String mkImage(String s) {return mkPicture(s, 300);}
    private String mkIcon(String s) {return mkPicture(s, 50);}
    private String mkPicture(String s, int size) {
        return String.format("https://robohash.org/%s.png?size=%dx%d", name, size, size);
    }

    private boolean missing(String s) {return s == null || s.isBlank();}
    private boolean missing(int i) {return i < 0;}
    private boolean missing(float i) {return i < 0;}
}
