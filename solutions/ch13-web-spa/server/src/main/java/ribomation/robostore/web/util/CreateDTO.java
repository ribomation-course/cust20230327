package ribomation.robostore.web.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ribomation.robostore.domain.Robot;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateDTO {
    String name;
    String description;
    String city;
    String country;
    Float price;
    Integer count;

    public Robot toRobo() {
        return Robot.builder()
                .name(name)
                .description(description)
                .city(city)
                .country(country)
                .price(price)
                .count(count)
                .build()
                .complete();
    }
}
