package ribomation.robostore.web.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ribomation.robostore.domain.Robot;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDTO {
    Float price;
    Integer count;
    String city;
    String country;

    public Robot patch(Robot r) {
        if (price != null && price > 0) r.setPrice(price);
        if (count != null && count >= 0) r.setCount(count);
        if (city != null && !city.isBlank()) r.setCity(city);
        if (country != null && !country.isBlank()) r.setCountry(country);
        return r;
    }
}
