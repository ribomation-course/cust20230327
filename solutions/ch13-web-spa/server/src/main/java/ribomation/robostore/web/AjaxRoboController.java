package ribomation.robostore.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.domain.Robot;
import ribomation.robostore.jpa.JpaRobotDAO;
import ribomation.robostore.web.util.CreateDTO;
import ribomation.robostore.web.util.NotFound;
import ribomation.robostore.web.util.PaginationDTO;
import ribomation.robostore.web.util.UpdateDTO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/robo")
@CrossOrigin
public class AjaxRoboController {
    private final JpaRobotDAO dao;
    public AjaxRoboController(JpaRobotDAO dao) {
        this.dao = dao;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String,String> error(NotFound x) {
        return Map.of("name", x.name);
    }

    @GetMapping
    public List<Robot> list() {
        return dao.findAll(PageRequest.of(0, 8)).toList();
    }

    @GetMapping({"paginate/{action}", "paginate/{action}/{page}"})
    public PaginationDTO paginate(@PathVariable PageAction action, @PathVariable(required = false) Integer page) {
        var nav = Pagination.of(page, (int) dao.count()).paginate(action);
        var pr = PageRequest.of(nav.page(), nav.size());
        var items = dao.findAll(pr).toList();
        return new PaginationDTO(nav.page(), items);
    }

    @GetMapping("summary")
    public Map<String, Number> summary() {
        return Map.of(
                "count", dao.count(),
                "zero", dao.countByCountEquals(0),
                "total", dao.totalValue()
        );
    }

    @GetMapping("search")
    public List<Robot> search(@RequestParam String phrase) {
        return dao.findFirst5ByNameLikeIgnoreCase('%' + phrase + '%');
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Robot save(@RequestBody CreateDTO dto) {
        var name = dto.getName();
        dao.save(dto.toRobo());
        return dao.findById(name).orElseThrow(() -> new NotFound(name));
    }

    @GetMapping("/{name}")
    public Robot one(@PathVariable String name) {
        return dao.findById(name).orElseThrow(() -> new NotFound(name));
    }

    @PutMapping("/{name}")
    public Robot update(@PathVariable String name, @RequestBody UpdateDTO dto) {
        var obj = dao.findById(name).orElseThrow(() -> new NotFound(name));
        dao.save(dto.patch(obj));
        return dao.findById(name).orElseThrow();
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable String name) {
        dao.deleteById(name);
    }

}
