package ribomation.robostore.web.util;

import ribomation.robostore.domain.Robot;
import java.util.List;

public record PaginationDTO(Integer page, List<Robot> items) {
}
