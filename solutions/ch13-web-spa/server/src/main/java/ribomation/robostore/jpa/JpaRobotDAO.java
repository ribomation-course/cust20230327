package ribomation.robostore.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ribomation.robostore.domain.Robot;

import java.util.List;

public interface JpaRobotDAO extends JpaRepository<Robot, String> {
    int countByCountEquals(int value);

    @Query("select sum(t.price * t.count) from Robot t")
    float totalValue();

    List<Robot> findFirst5ByNameLikeIgnoreCase(String phrase);
}
