package ribomation.robostore.web.util;

public class NotFound extends RuntimeException {
    public final String name;

    public NotFound(String name) {
        this.name = name != null ? name : "??";
    }
}
