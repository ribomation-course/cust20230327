import {Component} from '@angular/core';
import {NgFor} from '@angular/common';
import {Nav, routes} from '../../routes'
import {RouterLink, RouterLinkActive} from "@angular/router";
import {RobotService} from "../../services/robot.service";


@Component({
    selector: 'navbar',
    standalone: true,
    imports: [NgFor, RouterLink, RouterLinkActive],
    templateUrl: './navbar.widget.html',
    styles: [],
})
export class NavbarWidget {
    roboLogo = 'https://robohash.org/three.png?size=32x32';
    navs: Nav[] = routes
        .filter(r => !!r.data && !!r.data['nav'])
        .map(r => {
            // @ts-ignore
            const d = r.data['nav'] || {};
            return {
                uri: r.path,
                text: d.text,
                icon: d.icon
            } as Nav;
        });

    constructor(private searchSvc: RobotService) {}

    search(phrase: string) {
        this.searchSvc.search(phrase);
    }

}
