import {Component, ViewChild} from '@angular/core';
import {FormGroup, FormsModule} from "@angular/forms";
import {Router, RouterLink} from "@angular/router";
import {Robot} from "../../domain/robot.domain";
import {RobotService} from "../../services/robot.service";

@Component({
    selector: 'app-create',
    standalone: true,
    imports: [FormsModule, RouterLink],
    templateUrl: './create.page.html',
    styles: []
})
export class CreatePage {
    robot: Robot = {
        name: '',
        description: '',
        uid: '',
        city: '',
        country: '',
        icon: '',
        image: '',
        price: 0,
        count: 0
    };
    @ViewChild('form') form: FormGroup | undefined;

    constructor(private robotSvc: RobotService, private router: Router) {
    }

    async save() {
        await this.robotSvc.create(this.robot);
        await this.router.navigate(['/show', this.robot.name]);
    }
}
