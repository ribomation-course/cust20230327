import {Component, OnInit} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import "@angular/common/locales/global/sv";
import {RobotService, Summary} from "../../services/robot.service";


@Component({
    selector: 'app-summary',
    standalone: true,
    imports: [CurrencyPipe],
    templateUrl: './summary.page.html',
    styleUrls: ['./summary.page.css']
})
export class SummaryPage implements OnInit {
    summary: Summary = {count: 0, total: 0, zero: 0};

    constructor(private robotSvc: RobotService) {
    }

    async ngOnInit() {
        this.summary = await this.robotSvc.summary();
    }
}
