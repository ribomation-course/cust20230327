package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import ribomation.robostore.cli.RoboCommands;

import java.util.Properties;

public class Runner {
    private final ApplicationContext ctx;
    private final AppInfo app;
    private final Properties buildData;
    private final Properties gitData;

    public Runner(ApplicationContext ctx, AppInfo app, Properties buildData, Properties gitData) {this.ctx = ctx;
        this.app = app;
        this.buildData = buildData;
        this.gitData = gitData;
    }

    public void run(String[] args) {
        System.out.println(app);
        System.out.println("------");

        var cmd = ctx.getBean(RoboCommands.class);
        if (args.length == 0) {
            cmd.summary();
            cmd.help();
        } else {
            switch (args[0]) {
                case "build" -> printBuild();
                case "help" -> cmd.help();
                case "summary" -> cmd.summary();
                case "list" -> cmd.list();
                case "first" -> cmd.listFirst();
                case "last" -> cmd.listLast();
                case "next" -> cmd.listNext(Integer.valueOf(args[1]));
                case "prev" -> cmd.listPrev(Integer.valueOf(args[1]));

                case "one" -> cmd.one(args[1]);
                case "delete" -> cmd.delete(args[1]);
                case "search" -> cmd.search(args[1]);

                case "update" ->
                        cmd.update(args[1], args[2], args[3], Float.valueOf(args[4]), Integer.valueOf(args[5]));
                case "create" ->
                        cmd.create(args[1], args[2], args[3], args[4], Float.valueOf(args[5]), Integer.valueOf(args[6]));
            }
        }
    }

    public void printBuild() {
        print(buildData);
        print(gitData);
    }

    public void print(Properties p) {
        p.forEach((name,value) -> System.out.printf("%s = %s%n", name, value));
    }
}

