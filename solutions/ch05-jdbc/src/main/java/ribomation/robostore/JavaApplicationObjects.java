package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ribomation.robostore.cli.RoboCommands;

@Deprecated
public class JavaApplicationObjects extends ApplicationObjects {
    private final ApplicationContext ctx;

    public JavaApplicationObjects() {
        ctx = new AnnotationConfigApplicationContext(Beans.class);
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }
}
