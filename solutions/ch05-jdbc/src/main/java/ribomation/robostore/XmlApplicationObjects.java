package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.robostore.cli.RoboCommands;

@Deprecated
public class XmlApplicationObjects extends ApplicationObjects {
    private final ApplicationContext ctx;

    public XmlApplicationObjects(String xmlResource) {
        ctx = new ClassPathXmlApplicationContext(xmlResource);
    }

    public XmlApplicationObjects() {
        this("/beans.xml");
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }
}
