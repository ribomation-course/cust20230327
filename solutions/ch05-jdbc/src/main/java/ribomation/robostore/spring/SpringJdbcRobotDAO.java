package ribomation.robostore.spring;

import org.springframework.stereotype.Repository;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.JdbcRobotDAO;

//@Repository
@Deprecated
public class SpringJdbcRobotDAO extends JdbcRobotDAO {
    public SpringJdbcRobotDAO(SpringH2DatasourceFactory factory, BeanPreparator beanPreparator) {
        super(factory.create(), beanPreparator);
    }
}
