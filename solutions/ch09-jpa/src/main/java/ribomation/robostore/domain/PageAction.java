package ribomation.robostore.domain;

public enum PageAction {
    first, prev, next, last
}
