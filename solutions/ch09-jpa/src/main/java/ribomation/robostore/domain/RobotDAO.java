package ribomation.robostore.domain;

import java.util.List;
import java.util.Optional;

@Deprecated
public interface RobotDAO {
    String TBL = "ROBO_STORE";

    int count();
    float totalValue();

    default List<Robot> list() {return list(0, 10);}
    List<Robot> list(int offset, int rows);
    List<Robot> search(String phrase);
    List<Robot> outOfStock();

    Optional<Robot> byName(String name);
    boolean exists(String name);

    void insert(Robot robo);
    void insert(List<Robot> list);
    void update(String name, Robot robo);
    void delete(String name);
}
