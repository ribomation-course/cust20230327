package ribomation.robostore.jpa;
import org.jline.terminal.Terminal;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ribomation.robostore.AppInfo;
import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.domain.Robot;
import java.util.Properties;
import java.util.function.Consumer;

@ShellComponent
public class JpaRoboCommands {
    private final JpaRobotDAO dao;
    private final Terminal terminal;
    private final AppInfo app;
    private final Properties buildData;

    public JpaRoboCommands(JpaRobotDAO dao, Terminal terminal, AppInfo app, Properties buildData) {
        this.dao = dao;
        this.terminal = terminal;
        this.app = app;
        this.buildData = buildData;
        terminal.writer().println(app);
    }

    @ShellMethod(value = "Prints out build & git meta-data", group = "Misc.")
    public void build() {
        var out = terminal.writer();
        out.println("-- Build --");
        buildData.forEach((name, value) ->
                out.printf("%s = %s%n", name, value));
    }

    @ShellMethod(value = "Prints a summary of the DB content", group = "Misc.")
    public void summary() {
        var out = terminal.writer();
        out.printf("# products    : %d%n", dao.count());
        out.printf("# out of stock: %d%n", dao.countByCountEquals(0));
        out.printf("total value   : %.2f kr%n", dao.totalValue());
    }

    @ShellMethod(value = "List the first 10 rows", group = "list")
    public void list() {
        dao.findAll(Pageable.ofSize(10)).forEach(terminal.writer()::println);
    }

    @ShellMethod(value = "Show robo(s) with partial name", group = "read")
    public void search(String phrase) {
        dao.findFirst5ByNameLikeIgnoreCase('%'+phrase+'%').forEach(terminal.writer()::println);
    }

    @ShellMethod(value = "Show robo with the name", group = "read")
    public void one(String name) {
        dao.findById(name).ifPresent(terminal.writer()::println);
    }

    @ShellMethod(value = "Delete robo with name", group = "crud")
    public void delete(String name) {
        if (dao.existsById(name)) {
            dao.deleteById(name);
        } else {
            terminal.writer().printf("Not found: %s%n", name);
        }
    }

    @ShellMethod(value = "Create new robo", group = "crud")
    public void create(String name, String description, String city, String country, Float price, Integer count) {
        if (dao.existsById(name)) {
            terminal.writer().printf("Robo '%s' already exists%n", name);
            return;
        }

        var robo = Robot.builder();
        robo.name(name);
        if (description != null) robo.description(description);
        if (city != null) robo.city(city);
        if (country != null) robo.country(country);
        if (price != null) robo.price(price);
        if (count != null) robo.count(count);
        var obj = robo.build().complete();
        dao.save(obj);

        dao.findById(name).ifPresent(terminal.writer()::println);
    }

    @ShellMethod(value = "Update robo with name", group = "crud")
    public void update(String name,
                       @ShellOption(defaultValue = ShellOption.NULL) String city,
                       @ShellOption(defaultValue = ShellOption.NULL) String country,
                       @ShellOption(defaultValue = ShellOption.NULL) Float price,
                       @ShellOption(defaultValue = ShellOption.NULL) Integer count)
    {
        var maybeRobo = dao.findById(name);
        if (maybeRobo.isEmpty()) {
            terminal.writer().printf("Not found: %s%n", name);
            return;
        }

        var robo = maybeRobo.get();
        if (city != null) robo.setCity(city);
        if (country != null) robo.setCountry(country);
        if (price != null) robo.setPrice(price);
        if (count != null) robo.setCount(count);
        dao.save(robo);

        dao.findById(name).ifPresent(terminal.writer()::println);
    }

    private void paginate(PageAction action, Integer page) {
        var nav = Pagination.of(page, (int) dao.count()).paginate(action);
        var pr = PageRequest.of(nav.page() - 1, nav.size());
        dao.findAll(pr).forEach(terminal.writer()::println);
        terminal.writer().printf("Page: %d%n", nav.page());
    }

    @ShellMethod(value = "List the first N rows", group = "list")
    public void first() {
        paginate(PageAction.first, null);
    }

    @ShellMethod(value = "List the previous N rows", group = "list")
    public void prev(Integer page) {paginate(PageAction.prev, page);}

    @ShellMethod(value = "List the next N rows", group = "list")
    public void next(Integer page) {
        paginate(PageAction.next, page);
    }

    @ShellMethod(value = "List the last N rows", group = "list")
    public void last() {
        paginate(PageAction.last, null);
    }

}
