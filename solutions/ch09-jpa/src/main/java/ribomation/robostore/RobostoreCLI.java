package ribomation.robostore;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.jline.PromptProvider;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

@SpringBootApplication
public class RobostoreCLI {
    public static void main(String[] args) {
        SpringApplication.run(RobostoreCLI.class, args);
    }

//    @Bean
//    public CommandLineRunner invokeCommand(ApplicationContext ctx) {
//        return args -> {
//            var runner = new Runner(ctx);
//            runner.run(args);
//        };
//    }

    @Bean
    public Properties buildData() {
        return loadProps("/META-INF/build-info.properties");
    }

    @Bean
    public Properties gitData() {
        return loadProps("/git.properties");
    }

    public Properties loadProps(String resourcePath) {
        var is = getClass().getResourceAsStream(resourcePath);
        if (is != null) {
            try {
                var props = new Properties();
                props.load(is);
                return props;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        throw new RuntimeException("cannot open " + resourcePath);
    }

    @Bean
    public PromptProvider shellPrompt() throws UnknownHostException {
        var name = "RoboStore";
        var host = InetAddress.getLocalHost().getHostName();
        return () -> new AttributedString(String.format("%s@%s> ", name, host),
                AttributedStyle.DEFAULT.foreground(AttributedStyle.RED));
    }

}
