package ribomation.robostore.cli;

import org.jline.terminal.Terminal;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Service;
import ribomation.robostore.AppInfo;
import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.domain.Robot;
import ribomation.robostore.domain.RobotDAO;

import java.io.PrintWriter;
import java.util.Properties;

@Deprecated
//@Service
//@ShellComponent
public class RoboCommands {
    private final RobotDAO dao;
    private final Terminal terminal;
    private final AppInfo app;
    private final Properties buildData;
    private final Properties gitData;

    public RoboCommands(RobotDAO dao, Terminal terminal, AppInfo app, Properties buildData, Properties gitData) {
        this.dao = dao;
        this.terminal = terminal;
        this.app = app;
        this.buildData = buildData;
        this.gitData = gitData;
        terminal.writer().println(app);
    }

    @ShellMethod(value="Prints out build & git meta-data", group = "Misc.")
    public void build() {
        var out = terminal.writer();
        out.println("-- Build --");
        buildData.forEach((name,value) ->
                out.printf("%s = %s%n", name, value));
        out.println("-- Git --");
        gitData.forEach((name,value) ->
                out.printf("%s = %s%n", name, value));
    }

    @ShellMethod(value = "Prints a summary of the DB content", group = "Misc.")
    public void summary() {
        var out = terminal.writer();
        out.printf("# products    : %d%n", dao.count());
        out.printf("# out of stock: %d%n", dao.outOfStock().size());
        out.printf("total value   : %.2f kr%n", dao.totalValue());
    }

    @ShellMethod(value = "List the first 10 rows", group = "list")
    public void list() {
        dao.list().forEach(terminal.writer()::println);
    }

    private void paginate(PageAction action, Integer page) {
        var nav = Pagination.of(page, dao.count()).paginate(action);
        dao.list(nav.offset(), nav.size()).forEach(terminal.writer()::println);
        terminal.writer().printf("Page: %d%n", nav.page());
    }

    @ShellMethod(value = "List the first N rows", group = "list")
    public void listFirst() {
        paginate(PageAction.first, null);
    }

    @ShellMethod(value = "List the previous N rows", group = "list")
    public void listPrev(Integer page) {paginate(PageAction.prev, page);}

    @ShellMethod(value = "List the next N rows", group = "list")
    public void listNext(Integer page) {
        paginate(PageAction.next, page);
    }

    @ShellMethod(value = "List the last N rows", group = "list")
    public void listLast() {
        paginate(PageAction.last, null);
    }

    @ShellMethod(value = "Show robo(s) with partial name", group = "read")
    public void search(String phrase) {
        dao.search(phrase).stream().limit(15).forEach(terminal.writer()::println);
    }

    @ShellMethod(value = "Show robo with the name", group = "read")
    public void one(String name) {
        dao.byName(name).ifPresent(terminal.writer()::println);
    }

    @ShellMethod(value = "Delete robo with name", group = "crud")
    public void delete(String name) {
        if (dao.exists(name)) {
            dao.delete(name);
        } else {
            terminal.writer().printf("Not found: %s%n", name);
        }
    }

    @ShellMethod(value = "Update robo with name", group = "crud")
    public void update(String name, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            var robo = dao.byName(name).orElseThrow();
            if (city != null) robo.setCity(city);
            if (country != null) robo.setCountry(country);
            if (price != null) robo.setPrice(price);
            if (count != null) robo.setCount(count);

            dao.update(name, robo);
            dao.byName(name).ifPresent(terminal.writer()::println);
        } else {
            terminal.writer().printf("Not found: %s%n", name);
        }
    }

    @ShellMethod(value = "Create new robo", group = "crud")
    public void create(String name, String description, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            terminal.writer().printf("Robo '%s' already exists%n", name);
            return;
        }

        var robo = Robot.builder();
        robo.name(name);
        if (description != null) robo.description(description);
        if (city != null) robo.city(city);
        if (country != null) robo.country(country);
        if (price != null) robo.price(price);
        if (count != null) robo.count(count);

        dao.insert(robo.build().complete());
        dao.byName(name).ifPresent(terminal.writer()::println);
    }
}
