package ribomation.robostore.jdbc;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ribomation.robostore.domain.Robot;
import ribomation.robostore.domain.RobotDAO;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

@Deprecated
//@Repository
public class JdbcTemplateRobotDAO implements RobotDAO {
    private final JdbcTemplate jdbc;
    private final BeanPreparator mapper;

    public JdbcTemplateRobotDAO(JdbcTemplate jdbc, BeanPreparator mapper) {
        this.jdbc = jdbc;
        this.mapper = mapper;
    }

    @Override
    public int count() {
        var sql = format("select count(*) from %s", TBL);
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public float totalValue() {
        var sql = format("select sum(t.price * t.count) from %s t", TBL);
        return jdbc.queryForObject(sql, Float.class);
    }

    @Override
    public List<Robot> outOfStock() {
        var sql = format("select * from %s where count = 0", TBL);
        return jdbc.query(sql, mapper);
    }

    @Override
    public List<Robot> search(String phrase) {
        var sql = format("select * from %s where name ilike ?", TBL);
        return jdbc.query(sql, mapper, '%' + phrase + '%');
    }

    @Override
    public List<Robot> list(int offset, int rows) {
        var sql = format("select * from %s offset ? rows fetch first ? rows only", TBL);
        return jdbc.query(sql, mapper, offset, rows);
    }

    @Override
    public Optional<Robot> byName(String name) {
        var sql = format("select * from %s where name = ?", TBL);
        try {
            var obj = jdbc.queryForObject(sql, mapper, name);
            return Optional.ofNullable(obj);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean exists(String name) {
        return byName(name).isPresent();
    }

    @Override
    public void insert(Robot robo) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        mapper.setRobo(robo);
        jdbc.update(sql, mapper);
    }

    @Override
    public void insert(List<Robot> list) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        jdbc.batchUpdate(sql, list, 10, mapper);
    }

    @Override
    public void update(String name, Robot robo) {
        var sql = format("update %s set price=?, count=?, city=?, country=? where name = ?", TBL);
        jdbc.update(sql, robo.getPrice(), robo.getCount(), robo.getCity(), robo.getCountry(), name);
    }

    @Override
    public void delete(String name) {
        var sql = format("delete from %s where name = ?", TBL);
        jdbc.update(sql, name);
    }

    private String columns() {
        return String.join(",", Robot.names);
    }

    private String marks() {
        return Stream.of(Robot.names).map(__ -> "?").collect(Collectors.joining(","));
    }
}
