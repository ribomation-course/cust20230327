package ribomation.robostore.jdbc;

import org.h2.jdbcx.JdbcConnectionPool;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class H2DatasourceFactory {
    private String url;
    private String usr;
    private String pwd;

    public H2DatasourceFactory() {
        this("/db.properties");
    }

    public H2DatasourceFactory(String dbPropertiesPath) {
        try {
            var p = new Properties();
            p.load(getClass().getResourceAsStream(dbPropertiesPath));
            init(p.getProperty("jdbc.url"), p.getProperty("jdbc.username"), p.getProperty("jdbc.password"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public H2DatasourceFactory(String url, String usr, String pwd) {
        init(url, usr, pwd);
    }

    public DataSource create() {
        return JdbcConnectionPool.create(url, usr, pwd);
    }

    private void init(String url, String usr, String pwd) {
        this.url = url;
        this.usr = usr;
        this.pwd = pwd;
    }
}
