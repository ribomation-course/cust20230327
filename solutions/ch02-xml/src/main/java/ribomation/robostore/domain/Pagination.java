package ribomation.robostore.domain;

public class Pagination {
    public static final int DEFAULT_SIZE = 10;
    private final int page;
    private final int pages;
    private final int size;

    protected Pagination(int page, int pages, int size) {
        this.page = clamp(1, page, pages);
        this.pages = pages;
        this.size = size;
    }

    public static Pagination of(Integer page, Integer size, Integer rows) {
        if (page == null) page = 1;
        if (size == null) size = DEFAULT_SIZE;
        if (rows == null) rows = DEFAULT_SIZE;
        return new Pagination(page, (int) (rows / size), size);
    }

    public static Pagination of(Integer page, Integer rows) {
        return of(page, null, rows);
    }

    public int page() {
        return page;
    }

    public int offset() {
        return (page - 1) * size;
    }

    public int size() {
        return size;
    }

    public Pagination first() {
        return new Pagination(1, pages, size);
    }

    public Pagination last() {
        return new Pagination(pages, pages, size);
    }

    public Pagination next() {
        return new Pagination(page + 1, pages, size);
    }

    public Pagination previous() {
        return new Pagination(page - 1, pages, size);
    }

    public Pagination paginate(PageAction action) {
        if (action == null) action = PageAction.first;
        return switch (action) {
            case first -> first();
            case prev -> previous();
            case next -> next();
            case last -> last();
        };
    }

    private int clamp(int lb, int ub, int val) {
        if (val < lb) return lb;
        if (val > ub) return ub;
        return val;
    }
}
