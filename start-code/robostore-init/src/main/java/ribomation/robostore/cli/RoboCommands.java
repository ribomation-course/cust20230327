package ribomation.robostore.cli;

import ribomation.robostore.domain.PageAction;
import ribomation.robostore.domain.Pagination;
import ribomation.robostore.domain.Robot;
import ribomation.robostore.domain.RobotDAO;

import java.io.PrintWriter;

public class RoboCommands {
    private final RobotDAO dao;
    private final PrintWriter out;

    public RoboCommands(RobotDAO dao) {
        this.dao = dao;
        this.out = new PrintWriter(System.out, true);
    }

    public void summary() {
        out.printf("# products    : %d%n", dao.count());
        out.printf("# out of stock: %d%n", dao.outOfStock().size());
        out.printf("total value   : %.2f kr%n", dao.totalValue());
    }
    public void list() {
        dao.list().forEach(out::println);
    }

    private void paginate(PageAction action, Integer page) {
        var nav = Pagination.of(page, dao.count()).paginate(action);
        dao.list(nav.offset(), nav.size()).forEach(out::println);
        out.printf("Page: %d%n", nav.page());
    }
    public void listFirst() {
        paginate(PageAction.first, null);
    }
    public void listPrev(Integer page) {
        paginate(PageAction.prev, page);
    }
    public void listNext(Integer page) {
        paginate(PageAction.next, page);
    }
    public void listLast() {
        paginate(PageAction.last, null);
    }

    public void search(String phrase) {
        dao.search(phrase).stream().limit(15).forEach(out::println);
    }
    public void one(String name) {
        dao.byName(name).ifPresent(out::println);
    }

    public void delete(String name) {
        if (dao.exists(name)) {
            dao.delete(name);
        } else {
            out.printf("Not found: %s%n", name);
        }
    }
    public void update(String name, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            var robo = dao.byName(name).orElseThrow();
            if (city != null) robo.setCity(city);
            if (country != null) robo.setCountry(country);
            if (price != null) robo.setPrice(price);
            if (count != null) robo.setCount(count);

            dao.update(name, robo);
            dao.byName(name).ifPresent(out::println);
        } else {
            out.printf("Not found: %s%n", name);
        }
    }
    public void create(String name, String description, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            out.printf("Robo '%s' already exists%n", name);
            return;
        }

        var robo = Robot.builder();
        robo.name(name);
        if (description != null) robo.description(description);
        if (city != null) robo.city(city);
        if (country != null) robo.country(country);
        if (price != null) robo.price(price);
        if (count != null) robo.count(count);

        dao.insert(robo.build().complete());
        dao.byName(name).ifPresent(out::println);
    }
}


