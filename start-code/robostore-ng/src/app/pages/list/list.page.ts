import {Component, OnInit, OnDestroy} from '@angular/core';
import {CurrencyPipe, NgFor, NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import "@angular/common/locales/global/sv";
import {Subscription} from "rxjs";
import {RobotService, Pagination} from "../../services/robot.service";
import {Robot} from "../../domain/robot.domain";


@Component({
    selector: 'app-list',
    standalone: true,
    imports: [NgFor, NgIf, RouterLink, CurrencyPipe],
    templateUrl: './list.page.html',
    styles: []
})
export class ListPage implements OnInit, OnDestroy {
    subs: Subscription | undefined;
    current: Pagination = {items: [], page: 0};

    constructor(private robotSvc: RobotService) {
        this.subs = this.robotSvc.channel$()
            .subscribe(res => this.current = res)
    }

    async ngOnInit() {
        await this.first();
    }

    ngOnDestroy(): void {
        if (!!this.subs) {
            this.subs.unsubscribe();
        }
    }

    async first() {
        this.current = await this.robotSvc.first();
    }

    async prev() {
        this.current = await this.robotSvc.prev(this.current.page);
    }

    async next() {
        this.current = await this.robotSvc.next(this.current.page);
    }

    async last() {
        this.current = await this.robotSvc.last();
    }

    trackByFn(index: number, item: Robot) {
        return item.name;
    }

}
