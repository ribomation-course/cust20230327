#!/usr/bin/env bash
set -eux

java -cp ./lib/h2.jar org.h2.tools.Server -baseDir ./db -ifNotExists -tcp -tcpPort 9100 -web -webPort 9000
