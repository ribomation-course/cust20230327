#!/usr/bin/env bash
set -eux

java -cp ./lib/h2.jar org.h2.tools.RunScript -url jdbc:h2:tcp://localhost:9100/robostore -user sa -password '' -checkResults -script ./sql/schema.sql 
java -cp ./lib/h2.jar org.h2.tools.RunScript -url jdbc:h2:tcp://localhost:9100/robostore -user sa -password '' -checkResults -script ./sql/data.sql 
