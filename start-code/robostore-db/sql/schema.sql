drop table if exists robo_store;

create table robo_store (
    name            varchar(32) primary key,
    description     varchar(128),
    city            varchar(32),
    country         varchar(32),
    price           float,
    count           integer,
    uid             varchar(32),
    icon            varchar(128),
    image           varchar(128)
);
