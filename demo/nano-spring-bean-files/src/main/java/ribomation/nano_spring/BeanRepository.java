package ribomation.nano_spring;

import ribomation.nano_spring.impl.BeanRepositoryImpl;
import ribomation.nano_spring.impl.PropertiesLoader;

public interface BeanRepository {

    <T> T getBean(String name, Class<T> T);

    static BeanRepository createFromResource(String resource) {
        var repo   = new BeanRepositoryImpl();
        var loader = new PropertiesLoader(repo);
        loader.load(resource);
        return repo;
    }
}

