package ribomation.nano_spring.impl;

import ribomation.nano_spring.BeanRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BeanRepositoryImpl implements BeanRepository {
    private final Map<String, BeanDefinition> definitions = new HashMap<>();
    private final Map<String, Object> beans = new HashMap<>();

    public void register(String beanName, String className, List<String> constructorArgs) {
        if (!definitions.containsKey(beanName)) {
            definitions.put(beanName, new BeanDefinition(className, constructorArgs));
        }
    }

    @Override
    public <T> T getBean(String name, Class<T> type) {
        var bean= getBean(name);
        return type.cast(bean);
    }

    public Object getBean(String name) {
        if (beans.containsKey(name)) {
            return beans.get(name);
        }
        var def = definitions.get(name);
        if (def == null) {
            throw new RuntimeException("bean not found: name=" + name);
        }
        var bean = instantiate(def);
        beans.put(name, bean);
        return bean;
    }

    public Object instantiate(BeanDefinition def) {
        try {
            var constructorValues = def.constructorArgs.stream()
                    .map(arg -> {
                        if (arg.startsWith("@")) {
                            return getBean(arg.substring(1));
                        } else {
                            return arg;
                        }
                    })
                    .collect(Collectors.toList());

            var constructorTypes = constructorValues.stream()
                    .map(Object::getClass)
                    .collect(Collectors.toList());

            var beanClass   = Class.forName(def.beanClass);
            var constructor = beanClass.getConstructor(constructorTypes.toArray(new Class[0]));
            return constructor.newInstance(constructorValues.toArray());
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }

    Map<String, BeanDefinition> getDefinitions() {
        return definitions;
    }

    Map<String, Object> getBeans() {
        return beans;
    }

}

