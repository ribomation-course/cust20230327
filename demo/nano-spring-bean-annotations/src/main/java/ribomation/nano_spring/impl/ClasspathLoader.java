package ribomation.nano_spring.impl;

import ribomation.nano_spring.Bean;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class ClasspathLoader {
    private final BeanRepositoryImpl repo;

    public ClasspathLoader(BeanRepositoryImpl repo) {
        this.repo = repo;
    }

    public void load(String packageName) {
        try {
            var classNames = loadClassNames(packageName);
            var beanClasses = getBeanClassNames(packageName, classNames);
            beanClasses.forEach(repo::register);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    URL getUrl(String pkgName) {
        var clsLoader = Thread.currentThread().getContextClassLoader();
        return clsLoader.getResource(pkgName.replace('.', '/'));
    }

    List<String> loadClassNames(String packageName) throws URISyntaxException {
        var pkgNameRelativePath = packageName.replace('.', '/');
        var packageUrl          = getUrl(packageName);
        var baseDir             = packageUrl.toURI().getPath();
        final var B             = baseDir.length() - pkgNameRelativePath.length();
        final var C             = ".class".length();
        try (var entries = Files.walk(Path.of(packageUrl.toURI()))) {
            return entries
                    .filter(Files::isRegularFile)
                    .filter(p -> p.toString().endsWith(".class"))
                    .map(p -> p.toUri().getPath())
                    .map(p -> p.substring(B))
                    .map(p -> p.substring(0, p.length() - C))
                    .map(p -> p.replace('/', '.'))
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) { throw new RuntimeException(e); }
    }

    List<String> getBeanClassNames(String packageName, List<String> classFiles) {
        var packageUrl = getUrl(packageName);
        try (var clsLoader = new URLClassLoader(new URL[]{packageUrl})) {
            return classFiles.stream()
                    .map(name -> {
                        try {
                            return clsLoader.loadClass(name);
                        } catch (ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .filter(cls -> cls.getAnnotation(Bean.class) != null)
                    .map(Class::getName)
                    .collect(Collectors.toList());
        } catch (IOException e) { throw new RuntimeException(e); }
    }

}


