package ribomation.nano_spring.api.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static java.sql.Statement.RETURN_GENERATED_KEYS;
import static ribomation.nano_spring.api.impl.DQL_Generator.FIND;
import static ribomation.nano_spring.api.impl.DQL_Generator.FIND_ALL;

public class DaoHandler<DomainClass> implements InvocationHandler {
    private final DDL_Generator ddl = new DDL_Generator();
    private final DML_Generator dml = new DML_Generator();
    private final DQL_Generator dql = new DQL_Generator();
    private final Class<DomainClass> domainClass;
    private final GenericRowMapper<DomainClass> mapper;
    private final String tableName;
    private final String pkName;
    private final List<String> columnsExPk;
    private final List<String> columns;
    private final Connection connection;

    public DaoHandler(Class<DomainClass> domainClass, Connection connection) {
        this.domainClass = domainClass;
        this.mapper      = new GenericRowMapper<>(instantiate(domainClass));
        this.tableName   = dql.getTableName(domainClass);
        this.pkName      = dql.getPrimaryKey(domainClass);
        this.columns     = dql.getColumnNames(domainClass, true);
        this.columnsExPk = dql.getColumnNames(domainClass, false);
        this.connection  = connection;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final var name = method.getName();
        if (name.startsWith(FIND))     return findOne(name, args[0]);
        if (name.startsWith(FIND_ALL)) return findMany(name, args[0]);
        return switch (name) {
            case "createTable"    -> createTable();
            case "close"          -> close();
            case "count"          -> count();
            case "all"            -> all();
            case "insert"         -> insert((DomainClass) args[0]);
            case "remove"         -> remove((DomainClass) args[0]);
            case "withConnection" -> withConnection((Consumer<Connection>) args[0]);
            default               -> throw new UnsupportedOperationException(name);
        };
    }

    Object close() throws SQLException {
        connection.close();
        return null;
    }

    Object withConnection(Consumer<Connection> stmts) {
        stmts.accept(connection);
        return null;
    }

    Object createTable() throws SQLException {
        var sql = ddl.generateCreateTable(domainClass);
        try (var s = connection.createStatement()) {
            s.execute(sql);
        }
        return null;
    }

    int count() throws SQLException {
        var sql = dql.generateCountAll(tableName);
        try (var s = connection.createStatement(); var rs = s.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new IllegalStateException("no result from select count(*)");
        }
    }

    List<DomainClass> all() throws SQLException {
        var sql = dql.generateSelectAll(tableName);
        var lst = new ArrayList<DomainClass>();
        try (var s = connection.createStatement(); var rs = s.executeQuery(sql)) {
            while (rs.next()) {
                var obj = mapper.mapRow(rs);
                lst.add(obj);
            }
        }
        return lst;
    }

    Optional<DomainClass> findOne(String methodName, Object value) throws SQLException {
        var property = methodName.substring(FIND.length()).toLowerCase();
        var sql      = dql.generateSelect(tableName, columns, property);
        try (var ps = connection.prepareStatement(sql)) {
            ps.setObject(1, value);
            try (var rs = ps.executeQuery()) {
                return Optional.ofNullable(rs.next() ? mapper.mapRow(rs) : null);
            }
        }
    }

    List<DomainClass> findMany(String methodName, Object value) throws SQLException {
        var property = methodName.substring(FIND_ALL.length()).toLowerCase();
        var sql      = dql.generateSelect(tableName, columns, property);
        try (var ps = connection.prepareStatement(sql)) {
            ps.setObject(1, value);
            try (var rs = ps.executeQuery()) {
                var lst = new ArrayList<DomainClass>();
                while (rs.next()) lst.add(mapper.mapRow(rs));
                return lst;
            }
        }
    }

    Object insert(DomainClass domainObject) throws SQLException {
        var sql = dml.generateInsert(domainClass);
        try (var ps = connection.prepareStatement(sql, RETURN_GENERATED_KEYS)) {
            mapper.prepare(domainObject, columnsExPk, ps).executeUpdate();

            try (var rs = ps.getGeneratedKeys()) {
                if (rs.next()) {
                    var id         = rs.getObject(1);
                    var pkType     = domainClass.getDeclaredField(pkName).getType();
                    var setterName = mapper.toSetterName(pkName);
                    var setter     = domainClass.getMethod(setterName, pkType);
                    setter.invoke(domainObject, id);
                    return domainObject;
                } else throw new RuntimeException("no generated key");
            } catch (Exception e) {throw new RuntimeException(e);}
        }
    }

    Object remove(DomainClass domainObject) throws SQLException {
        var sql = dml.generateDelete(domainClass);
        try (var ps = connection.prepareStatement(sql)) {
            var getterName = mapper.toGetterName(pkName);
            var getter     = domainClass.getMethod(getterName);
            var pkValue    = getter.invoke(domainObject);

            ps.setObject(1, pkValue);
            ps.executeUpdate();

            return null;
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    DomainClass instantiate(Class<DomainClass> domainClass) {
        try {
            return domainClass.getConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
