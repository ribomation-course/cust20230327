package ribomation.nano_spring.jdbc;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ribomation.nano_spring.jdbc.H2.mkUrl;

class H2Test {

    @Test
    @DisplayName("memory database")
    void tst_mkUrl() {
        var url = mkUrl(H2.H2Types.memory, "dummy");
        assertEquals("jdbc:h2:mem:dummy", url);
    }

    @Test
    @DisplayName("tcp database")
    void tst2_mkUrl() {
        var url = mkUrl(H2.H2Types.socket, "dummy");
        assertEquals("jdbc:h2:tcp://localhost:9092/dummy", url);
    }

    @Test
    @DisplayName("folder database")
    void tst3_mkUrl() {
        var url = mkUrl(H2.H2Types.directory, "dummy");
        assertEquals("jdbc:h2:db_files/dummy", url);
    }

}