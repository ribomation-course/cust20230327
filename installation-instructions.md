# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## In short, you need the following
* Latest **Java** JDK, minimum version **17**
* An **IDE**, to write, compile and run your programs
* **Gradle** to build Spring applications. Possible to use Maven as well.
* **GIT** client
* **HTTP CLI** client

## In addition, it might want these tools
* **Node**, npm, npx, pnpm
* **HTTPie**

## SDKMAN
The most flexible way of install and maintain Java jdks and tools, is to use [SDKMAN](https://sdkman.io/).
It's a tool that requires BASH, such as GIT-BASH or Ubuntu@WSL. 

### Additional tasks for GIT-BASH @ Windows
Before you run the install script, you need to augment _Mingw64_ with
some missing but required programs. Mingw is a set of unix like command-line
programs for Windows, that _GIT for Windows_ install.

#### Step A
Find the installation dir of GIT. The default is `C:\Program Files\Git`. Open
the sub-dir `usr\bin`.

#### Step B
Browse to https://sourceforge.net/projects/gnuwin32/files/zip/3.0/ and
download `zip-3.0-bin.zip`. Extract the content of its `bin` dir into the dir
of step A above. You should find a few exe files, including `zip.exe`.

#### Step C
Proceed similarly with https://sourceforge.net/projects/gnuwin32/files/unzip/5.51-1/
and download and unpack `unzip-5.51-1-bin.zip`. You should find `unzip.exe` among
others.

### Installation of SDKMAN

    curl -s "https://get.sdkman.io" | bash

### Installation of Java JDK

    sdk install java 19.0.2-oracle

### Installation of Gradle

    sdk install gradle

### Installation of Spring CLI

    sdk install springboot


## Java JDK
The decent version of Java JDK installed, appropriate for Spring, such as v19,
or at least v17 (_required by Spring Boot 3_).
* [Java JDK Download](https://jdk.java.net/19/)

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA
  - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
  - https://code.visualstudio.com/download
* Eclipse
  - https://www.eclipse.org/ide/
* Something else, you already are familiar with

## Access to Spring Initializr Web
Ability to configure and download a project ZIP from `https://start.spring.io/`

## Build Tool
Need to have one of the build tools Maven or Gradle installed. However,
when generating a project via Spring Initializr, there is a wrapper shell
script to use.

* Gradle: `gradlew` / `gradle.bat`
* Maven: `mvnw` / `mvnw.cmd`

## GIT Client
* [Git Client Download](https://git-scm.com/downloads)

## HTTP CLI Client
Although, there are many http clients to use, we do recommend [HTTPie](https://httpie.io/).
Here is various ways of how to install it https://httpie.io/docs/cli/installation

## Node.js & NPM/NPX & PNPM
At the end of the course, there is an exercise involving a single-page app client.

If you want to perform these exercises, by building the client,
you need to have Node.js installed as well.
_N.B._, it's possible to complete the exercises without Node.js, by just copy the
pre-built files. However, it would not be as fun.

* [Node.js & NPM/NPX](https://nodejs.org